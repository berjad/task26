﻿Task 26 - Cookie Manager Class

Write a JavaScript class called CookieManager with 3 methods.

setCookie(name, data);
Set a cookie with the name as given argument and assign data given as argument. The cookie must expire in 15 minutes from the time it was set.

getCookie(name);
Return the value of a cookie based on the name given as an argument.

clearCookie(name);
Delete a cookie based on the name given as an argument.
