class CookieManager {

setCookie(name, data)  {
    let d = new Date();
    d.setTime(d.getTime() + (15 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + data + ";" + expires + ";path=/";
}

getCookie(name) {
   n = name + "=";
   let ca = document.cookie.split(';');
   for(let i = 0; i < ca.length; i++) {
       let c = ca[i];
       while (c.charAt(0) == ' ') {
           c = c.substring(1);
       }
       if(c.indexOf(n) == 0) {
           return c.substring(n.length, c.length);
       }
   } 
   return "";
}

clearCookie(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}


}
